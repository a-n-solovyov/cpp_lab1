//
// Created by alexander on 28/02/2020.
//

#include "Product.h"


Product::Product(string name, double price) {
    this->name = name;
    setPrice(price);
}

Product::Product() = default;

Product::Product(const Product &other) = default;

Product::~Product() = default;

Product::Product(string name, double price, string man, int count) : name{name}, price{price}, count{count},
                                                                         manufacturer{man} {

}

ostream& operator<<(ostream& os, const Product& p)
{
    string s = "<" + p.Name() + "> " + "Цена: " + to_string(p.Price()) + " Кол-во: " + to_string(p.Count())
                 + " Производитель: " + p.Manufacturer() + " " + p.getDateProduced();
    os << s;
    return os;
}

void Product::setPrice(double value) {
    if (value>=0)
        price = value;
}

void Product::setCount(int value) {
    if (value>=1)
        price = value;
}

string Product::getDateProduced() const {
    char buffer[80];
    tm *timeinfo = localtime(&date);
    strftime(buffer, sizeof(buffer), "%d-%m-%Y %H:%M:%S", timeinfo);
    string str = buffer;
    delete timeinfo;
    return str;
}





