//
// Created by alexander on 28/02/2020.
//
#include <ctime>
#include <string>
#include <iomanip>
#include <map>
#include <stack>

#ifndef LAB1_PRODUCT_H
#define LAB1_PRODUCT_H

using namespace ::std;
typedef std::map<string, tuple<int, double>> my_map;


class Product {
private:
    double price{};
    time_t date = time(0);
    int count = 1;
    string manufacturer;
    string name;

    friend my_map getAvgPrices(stack<Product> stack);

public:
    string Name() const { return name; }

    string Manufacturer() const { return manufacturer; }

    int Count() const { return count; }

    double Price() const { return price; }

    time_t DateProduced() const { return date; }

    void setPrice(double value);

    void setCount(int value);

    string getDateProduced() const;;

    Product();

    Product(string name, double price);

    Product(string name, double price, string man, int count = 1);

    Product(const Product &other);

    ~Product();

    friend ostream &operator<<(ostream &os, const Product &p);
};


#endif //LAB1_PRODUCT_H
