#include <iostream>
#include "Product.h"
#include <map>
#include <stack>
#include <tuple>

//1. Описать класс для хранения следующей информации:
//– название продукции,
//– стоимость за единицу,
//– количество,
//– дата выпуска,
//– изготовитель.
//2. Предусмотреть возможность добавления и вывод на экран объектов класса.
//Для хранения данных использовать стек.
//3. Определить среднюю стоимость за единицу продукции, по каждому
//изготовителю.
using namespace ::std;


void printMap(const my_map &m) {
    auto it = m.begin();

    while (it != m.end()) {
        std::cout << it->first << " - " << get<1>(it->second) / get<0>(it->second) << std::endl;
        it++;
    }
}

int main() {
	setlocale(LC_ALL, "Russian");
    stack<Product> s;

    auto p = Product("p1", 10, "man1");
    cout << p << endl;
    s.push(p);
    s.push(Product("p2", 100, "man1"));
    s.push(Product("p3", 45, "man2"));
    s.push(Product("p4", 33, "man2"));
    s.push(Product("p5", 100, "man3"));
    s.push(Product("p6", 100, "man3"));
    s.push(Product("p7", 150, "man3"));
    my_map prices_by_manufacturer = getAvgPrices(s);

    printMap(prices_by_manufacturer);
    return 0;
}

//friend
my_map getAvgPrices(stack<Product> s) {
    my_map res;
    while (!s.empty()) {
        Product p = s.top();
        s.pop();
        if (!p.manufacturer.empty()) {
            if (res.count(p.Manufacturer()) == 0) {
                res[p.Manufacturer()] = make_tuple(1, p.Price());
            } else {
                tuple<int, double> &value = res[p.Manufacturer()];
                get<0>(value)++;
                get<1>(value) += p.Price();
            }
        }
    }
    return res;
}
